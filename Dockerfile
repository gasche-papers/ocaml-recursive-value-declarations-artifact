FROM ocaml/opam2:debian-stable

########################################################################
## Bring the local OPAM repository into a recent known-working state.
########################################################################
RUN git pull && git checkout 8f5281a8

########################################################################
# Copy in the OPAM configuration for our custom switch
########################################################################
RUN mkdir /home/opam/opam-repository/packages/ocaml-variants/ocaml-variants.4.11.1+drecmodes
ADD opam  /home/opam/opam-repository/packages/ocaml-variants/ocaml-variants.4.11.1+drecmodes
RUN git add packages/ocaml-variants/ocaml-variants.4.11.1+drecmodes/opam
RUN git commit -m 'add drecmodes compiler' packages/ocaml-variants/ocaml-variants.4.11.1+drecmodes/opam

RUN opam remote add --set-default ocaml-beta git+https://github.com/ocaml/ocaml-beta-repository.git
RUN opam update

########################################################################
# Create the various switches that we'll use
########################################################################
RUN opam switch create 4.11.1+drecmodes # Latest public OCaml release instrumented with mode dumping
RUN opam switch create 4.05.0           # OCaml before our new check
RUN opam switch create 4.04.0+BER       # MetaOCaml before our new check
RUN opam switch create 4.07.1+BER       # MetaOCaml after our new check

########################################################################
# Install smlnj
########################################################################
RUN yes | sudo apt-get install smlnj

########################################################################
# Install F#
# See https://docs.microsoft.com/en-us/dotnet/core/install/linux-debian#debian-10-
########################################################################
RUN curl https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb  --output packages-microsoft-prod.deb
RUN sudo dpkg -i  packages-microsoft-prod.deb
RUN sudo apt-get update; sudo apt-get install -y apt-transport-https && sudo apt-get update && sudo apt-get install -y dotnet-sdk-3.1
RUN dotnet fsi < /dev/null || true


########################################################################
# Switch to the home directory and add the example code
########################################################################
WORKDIR /home/opam
ADD mfib.fsx mfib.fsx
ADD mfibprime.fsx mfibprime.fsx
ADD fibs.ml fibs.ml
RUN sudo apt-get install --yes python3
ADD data data
ADD algebraic_proofs.ml algebraic_proofs.ml
