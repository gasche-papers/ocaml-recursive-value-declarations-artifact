(* weird recursive types required for the example below to type-check *)
type 'a t = { self : 'a foo }
and 'a foo = Foo of ('a -> 'a t * 'a)

let rec x =
  let t = Foo (fun y -> (x, y)) in
  { self = t }

(* -drecmodes output:
x:Delay |- let t = Foo (fun y -> (x, y)) in { self = t } : Return
*)
