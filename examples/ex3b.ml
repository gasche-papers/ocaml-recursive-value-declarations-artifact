let test g =
  let rec f = g (fun x -> f x) in
  ignore f
(* -drecmodes output:
g:Dereference, f:Dereference |- g (fun x -> f x) : Return
File "./examples/ex3b.ml", line 2, characters 14-30:
2 |   let rec f = g (fun x -> f x) in
                  ^^^^^^^^^^^^^^^^
Error: This kind of expression is not allowed as right-hand side of `let rec'
*)
