let test g =
  let rec x =
    let t = fun y -> (x, y) in
    g t
  in ignore x

(* -drecmodes output:
g:Dereference, x:Dereference |- let t y = (x, y) in g t : Return
File "./examples/ex6b.ml", lines 3-4, characters 4-7:
3 | ....let t = fun y -> (x, y) in
4 |     g t
Error: This kind of expression is not allowed as right-hand side of `let rec'
*)
