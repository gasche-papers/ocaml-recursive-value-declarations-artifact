let test g =
  let rec f = fun x -> g (f x) in
  ignore f
(* -drecmodes output:
g:Delay, f:Delay |- fun x -> g (f x) : Return
*)
