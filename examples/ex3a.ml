(* simplified form of record encoding of object-oriented programs *)
type some_object = { self : some_object }

let test g =
  let rec x = g { self = x } in
  ignore x

(* -drecmodes output:
g:Dereference, x:Dereference |- g { self = x } : Return
File "./examples/ex3a.ml", line 5, characters 14-28:
5 |   let rec x = g { self = x } in
                  ^^^^^^^^^^^^^^
Error: This kind of expression is not allowed as right-hand side of `let rec'
*)
