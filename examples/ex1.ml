let rec x = x
(* -drecmodes output:
x:Return |- x : Return
File "./examples/ex1.ml", line 1, characters 12-13:
1 | let rec x = x
                ^
Error: This kind of expression is not allowed as right-hand side of `let rec'
*)
