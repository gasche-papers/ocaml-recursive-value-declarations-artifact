type 'a t = { self : 'a t }

let test y =
  let rec x =
    let rec t = x
    and u = { self = t }
    and v = fun () -> (u, y)
    in t
  in ignore x
(* -drecmodes output:
x:Return |- x : Return
t:Guard |- { self = t } : Return
y:Delay, u:Delay |- fun () -> (u, y) : Return
y:Delay, x:Return |-
let rec t = x
and u = { self = t }
and v () = (u, y) in t : Return
File "./examples/ex7b.ml", lines 5-8, characters 4-8:
5 | ....let rec t = x
6 |     and u = { self = t }
7 |     and v = fun () -> (u, y)
8 |     in t
Error: This kind of expression is not allowed as right-hand side of `let rec'
*)
