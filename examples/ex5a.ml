let test f =
  let rec x = f (fun () -> x) in
  ignore x
(* -drecmodes output:
f:Dereference, x:Dereference |- f (fun () -> x) : Return
File "./examples/ex5a.ml", line 2, characters 14-29:
2 |   let rec x = f (fun () -> x) in
                  ^^^^^^^^^^^^^^^
Error: This kind of expression is not allowed as right-hand side of `let rec'
*)
