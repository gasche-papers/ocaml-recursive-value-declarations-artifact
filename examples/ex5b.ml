let test f =
  let rec x = f (fun () -> 1) in
  ignore x
(* -drecmodes output:
f:Dereference |- f (fun () -> 1) : Return
*)
