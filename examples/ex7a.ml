type 'a t = { self : 'a t }

let test x u y =
  let rec t = x
  and u = { self = t }
  and v = fun () -> (u, y)
  in ignore (t, u, v)

(* -drecmodes output:
x:Return |- x : Return
t:Guard |- { self = t } : Return
y:Delay, u:Delay |- fun () -> (u, y) : Return
*)
