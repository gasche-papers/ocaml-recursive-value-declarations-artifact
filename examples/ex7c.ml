type 'a t = { self : 'a t }

let test y =
  let rec x =
    let rec t = x
    and u = { self = t }
    and v = fun () -> (u, y)
    in
    let _ = v in
    u
  in ignore x
(* -drecmodes output:
x:Return |- x : Return
t:Guard |- { self = t } : Return
y:Delay, u:Delay |- fun () -> (u, y) : Return
y:Delay, x:Guard |-
let rec t = x
and u = { self = t }
and v () = (u, y) in u : Return
*)
