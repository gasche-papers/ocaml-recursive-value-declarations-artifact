(* simplified form of record encoding of object-oriented programs *)
type some_object = { self : some_object }

let rec x = { self = x }

(* -drecmodes output:
x:Guard |- { self = x } : Return
*)
