let rec f = fun x -> f x

(* -drecmodes output:
f:Delay |- fun x -> f x : Return
*)
