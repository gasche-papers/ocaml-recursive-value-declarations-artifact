(* simplified form of record encoding of object-oriented programs *)
type some_object = { self : some_object }

let test g = 
  let rec x = { self = g x } in
  ignore x
(* -drecmodes output:
g:Dereference, x:Dereference |- { self = (g x) } : Return
File "./examples/ex4a.ml", line 5, characters 14-28:
5 |   let rec x = { self = g x } in
                  ^^^^^^^^^^^^^^
Error: This kind of expression is not allowed as right-hand side of `let rec'
*)
