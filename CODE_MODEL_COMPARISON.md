Our paper presents a formal model of a correctness check implemented
in the OCaml compiler:

https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml

(This links refers to a fixed-in-time version of the implementation,
corresponding to the release 4.11.1 of OCaml, the last published
release at the time of writing.)

We draw the reviewers' attention to the following salient
correspondences between the formal system in the paper and the
implementation in the code.

* The upper part of Figure 2 in the paper and lines 349-352 define the
  mode ordering, from more-permissive to more-demanding:

  ```
  Ignore < Delay < Guard < Return < Dereference
  ```
  This ordering is defined by the `Mode.rank` function in the code:
  https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml#L307-L317

* The mode composition shown in the middle and lower parts of Figure 2
  and in lines 355-360 is defined in `Mode.compose` in the code:
  https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml#L327-L342

* The paper describes environments and their merge operation in lines
  369-374; in the code these are defined in the `Env` module:
  https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml#L347-L427
  with the merge function defined as `Env.join`:
  https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml#L401-L406

* The main function that implements the inference algorithm in our
  implementation is `Rec_check.expression`, which accepts an expression
  and a mode as input and computes an environment:
  https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml#L512-L517

  In other words, for the judgement `Γ ⊢ e : m`, the
  `Rec_check.expression` function computes the environment `Γ`
  from the inputs `e`, `m`.

  Each of the inference rules in Figure 3 has one or more direct
  counterparts in the implementation, starting at line 513. For example,

     - the variable rule corresponds to the function `path`, which for
       simple identifiers `x` with mode `m` builds a singleton
       environment `x:m`:
       https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml#L889-L912

       For qualified identifiers `M.x`, `path` builds the environment
       `M:Dereference` that dereferences the parent module `M`.

     - The delay rule corresponds to the case for `Texp_function` (and
       `Texp_lazy`, but see the remarks in §7.5).  In OCaml, unlike in
       our formal system, functions are defined by cases; our
       algorithm composes the input mode with `Delay` for each case:
       https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml#L778-L789

       The `case` function, invoked from this code to compute the
       sub-environments, corresponds to the second clause rule in
       Figure 3.

     - The application rule composes the input mode with `Dereference`
       when computing environments for the subterms:
       https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml#L575-L584
       The final environment is the result of merging the environemnts
       for subterms.

     - The constructor rule corresponds to the case for `Texp_construct`:
       https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml#L601-L617

       There are various special cases in the implementation to handle
       special types of constructors in OCaml: in particular, an
       "unboxed" constructor which has no runtime representation must
       both be treated as a Dereference rather than a Guard.

       See also the case for array literals above (Texp_array), which
       uses the mode Guard in non-generic cases, but the mode
       Dereference in generic or float-array cases, as we described in §7.2.

     - The `rec` rule corresponds to the `Recursive` case of the
       `value_bindings` function:
       https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml#L1072-L1139
