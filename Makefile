all: build

build:
	docker build -t ocaml-recursive-values .

develop:
	docker run -it -v `pwd`:/local ocaml-recursive-values bash

run:
	docker run ocaml-recursive-values

save:
	docker save ocaml-recursive-values | gzip -c > ocaml-recursive-values.tar.gz

.PHONY: all build run
