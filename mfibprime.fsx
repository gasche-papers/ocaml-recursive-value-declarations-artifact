type ('k,'v) memofun = { f: 'k -> 'v;  mutable values: ('k * 'v) list  }
let remember t k =
    try snd (List.find (fst >> ((=) k)) t.values)
    with _ -> let v = t.f k in 
              t.values <- (k, v) :: t.values;
              v

let empty_table () = []
let rec mfib' = let mfibs' = { f = mfib'; values = empty_table () } in
                 fun x -> if x <= 1 then x
                           else remember mfibs' (x-1) + remember mfibs' (x-2)
