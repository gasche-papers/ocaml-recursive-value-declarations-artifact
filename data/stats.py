#!/usr/bin/env python3
import sys, re, pprint, os.path, subprocess, argparse

parser = argparse.ArgumentParser(description='Process letrec log entries.')
parser.add_argument('--hard', action="store_true",
                    help="keep only 'hard' entries (see test/test.ml)")
parser.add_argument('--entries', action="store_true",
                    help="print the filtered entries (only)")
parser.add_argument('--packages', action="store_true",
                    help="print the distinct opam packages (only)")
conf = parser.parse_args()

LOCLINE = re.compile(r'^File "(.*)", lines? ([0-9]+-?[0-9]*?), characters ([0-9]+-[0-9]+):')
PACKAGE = re.compile(r'^/.*/build/([^.]*).*')

def read_entries(lines):
    locline, loc, source = None, None, ''
    for line in lines:
        nextloc = LOCLINE.match(line)
        if nextloc is not None:
            if loc is not None:
                yield locline, loc, source
            locline, loc, source = line, nextloc.groups(), ''
        else:
            assert not line.startswith('File')
            source = (source + line) if source else line
    if loc is not None:
        yield locline, loc, source

def is_any(entry): return True

def is_hard(entry):
    source = entry[2]

    cmd = "test/_build/default/test.exe"
    if not os.path.isfile(cmd):
        print("you need to build the test executable first")
        exit(2)
    test = subprocess.run([cmd],
                          input=source.encode(),
                          capture_output=True)
    return (re.match(b"hard", test.stdout) is not None)

if conf.hard:
    filter_pred = is_hard
else:
    filter_pred = is_any

def sources(entries):
    return {source for _, _, source in entries}

def filenames(entries):
    return {filename for _, (filename,_,_), _ in entries}

def packages(entries):
    return {match.group(1) for match in
            (PACKAGE.match(name) for _, (name,_,_),_ in entries)
            if match is not None}

def packages_no_oasis(entries):
    return {match.group(1) for match in
            (PACKAGE.match(name) for _,(name,_,_),_ in entries
             if os.path.basename(name) != 'setup.ml')
            if match is not None}

def print_entries(entries):
    for locline, loc, source in entries:
        print(locline, end="")
        print(source, end="")

def print_packages(entries):
    for package in packages(entries):
        print(package)

def print_stats(entries):
    print('distinct entries:', len(sources(entries)))
    print('distinct filenames:', len(filenames(entries)))
    print('distinct packages:' , len(packages(entries)))
    print('distinct packages excluding OASIS setup:', \
        len(packages_no_oasis(entries)))

if __name__ == '__main__':
    entry_stream = read_entries(sys.stdin.readlines())
    entries = filter(filter_pred, entry_stream)
    if conf.entries:
        print_entries(entries)
    elif conf.packages:
        print_packages(list(entries))
    else:
        entries = list(entries)
        print_stats(entries)
