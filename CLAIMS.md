## Tools

Our Docker image contains various versions of the OCaml compiler that
can be used to demonstrate the claims in the paper:

  * 4.05.0

    The last major release of OCaml before we added our new check.

  * 4.11.1+drecmodes

    The most recent public release of OCaml, additionally enhanced
    with a new flag `-drecmodes` that prints out the inferred
    mode environment for each recursive binding.

  * 4.04.0+BER

    The last release of BER MetaOCaml before we added our new check.
  
  * 4.07.1+BER

    The first release of BER MetaOCaml before we added our new check.

These compiler versions are managed by OPAM, the OCaml package
manager, which makes it easy to switch from one compiler version to
another.  For example, to switch to OCaml 4.04.0+BER, type

    opam switch 4.04.0+BER    # select the compiler
    eval $(opam env)             # update the interactive shell

(The second line sets the values of various environment variables such
as `PATH`.)

The image also contains installations of SML/NJ and F#, about which we
make some in-passing claims in the paper.

Note: our implementation of the recursive check is included in 4.11.1
(and older releases of OCaml) as distributed by the official, upstream
OCaml compiler. 4.11.1+drecmodes is not an official release but an
experimental fork of 4.11.1 we created just for this artifact, to add
extra debug logic to observe the mode computations. The mode
computations themselves, the actual check, is untouched from the
upstream implementation. If you are curious, you can see the
+drecmodes patchset at

  https://github.com/ocaml/ocaml/compare/4.11.1...yallop:drecmodes

Clearly the only changes are debug-related.

In particular, all the experiments we propose below with
4.11.1+drecmodes can be reproduced with the upstream 4.11.1, the only
difference is that you would not observe the debug output for the mode
judgment. The same programs will be accepted or rejected.

### Tools without docker

If you don't use a Docker image, you can use a standard installation
of OPAM and get all switches, except for our experimental fork
4.11.1+drecmodes. For this one you can do

```
wget https://github.com/yallop/ocaml/archive/drecmodes.zip
unzip drecmodes.zip
cd ocaml-drecmodes
./configure
make world.opt -j5
```

The easiest way to run this compiler on an example file foo.ml is to
run, from the `ocaml-drecmodes/` directory:

```
./ocamlc.opt -nostdlib -I stdlib -drecmodes foo.ml
```

## Evidence for the claims in the paper

Below we provide evidence for the claims in the paper, section-by-section.

### §1 Introduction

This section of the paper motivates generalized recursion via a series
of examples, which we include in the file `fibs.ml`.  (The comments in
the file show where each function appears in the paper.)

Switch to the latest version of OCaml:

    opam switch 4.11.1+drecmodes
    eval $(opam env)

and run the file `fibs.ml`:

    ocaml fibs.ml

It should execute without warnings, printing

    OK!

The file also contains the commented-out erroneous definition `efibs`,
which is rejected by our system.  Uncommenting the definition and
running `ocaml fibs.ml` again prints an error message:

    File "./fibs.ml", line 61, characters 16-51:
    61 | let rec efibs = 0 :: 1 :: map2 (+) efibs (tl efibs) (* unsafe! *) 
                         ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    Error: This kind of expression is not allowed as right-hand side of `let rec'

We also mention (lines 55-57) that F#'s syntactic check supports
`mfib`, but not `mfib'`.  The files `mfib.fsx` and `mfibprime.fsx`
contain these functions respectively.  Run

    dotnet fsi mfib.fsx
   
to compile `mfib`; it should complete without errors.  Now run

    dotnet fsi mfibprime.fsx

to compile `mfib'`; it should fail with the following error message:

    /home/opam/mfibprime.fsx(9,36): warning FS0040: This and other recursive references to the object(s) being defined will be checked for initialization-soundness at runtime through the use of a delayed reference. This is because you are defining one or more recursive objects, rather than recursive functions. This warning may be suppressed by using '#nowarn "40"' or '--nowarn:40'.

    /home/opam/mfibprime.fsx(9,9): error FS0031: The value 'mfib'' will be evaluated as part of its own definition

We also mention (lines 61-63) that Standard ML allows only recursion
through syntactic function definitions.  Run

    sml

to start SML/NJ, and enter the following innocuous-seeming definition
at the prompt:

    val rec x = (1,2);

SML/NJ rejects the definition with an error message confirming that only
syntactic function definitions are supported:

    stdIn:1.10-1.19 Error: fn expression required on rhs of val rec

Finally, we mention (lines 83-85) that tools such as MetaOCaml, which
reuse the OCaml type-checker, benefit from our new check.  Start BER
MetaOCaml N104, which is based on a version of OCaml that does not
include our new check:

     opam switch 4.04.0+BER
     eval $(opam env)
     metaocaml

and enter the following at the prompt:

    .< let rec x = (1, 2) in x >.;;

MetaOCaml rejects the definition, since it uses an Standard ML-style
restriction:

    .< let rec x = (1, 2) in x >.;;
       ^^^^^^^^^^^^^^^^^^
    Error: Recursive let binding must be to a function

Now switch to BER MetaOCaml N107, which is based on a version of OCaml
that includes our new check:

     opam switch 4.07.1+BER
     eval $(opam env)
     metaocaml

and enter the expression at the prompt.  This later version of
MetaOCaml accepts this definition (and any other definition accepted
by our new check):

    - : (int * int) code = .<let rec x_1 = (1, 2) in x_1>. 

### §1.2 Generalized recursion in practice

We mention (lines 88-96) that we found 309 distinct examples of
generalized recursion in 74 OPAM packages.  The files in `data`
contain logs of these examples along with scripts to analyse the logs
(and various other related bits and pieces, including the patch we
used to instrument the compiler to collect the data).

The file `data/hard.log` lists each example along with the location
(filename, line numbers) in which it was found.

The file `stats.py` analyses the log and prints summary information.
Running `stats.py` as follows:

    cd data
    data/stats.py < data/hard.log

should print the following summary:

    distinct entries: 309
    distinct filenames: 558
    distinct packages: 74
    distinct packages excluding OASIS setup: 74

The following command finds the size of dependency cone mentioned in
lines 95-96 of the paper:

    opam list -A --depends-on "$(echo $(python3 data/stats.py --packages < data/hard.log) | sed "s/ /,/g")" | wc -l

(For a strictly accurate number, we should ignore the first couple of
lines; the number reported is too large by 2.)

The cone is slightly larger (size 1754) than the size reported in our
paper (size 1613), since the OPAM repository has grown since our
submission.

Reviewers may also wish to confirm our remark (lines 287-290) about
"negative" constructs

> Recursive values are a controversial feature as they break the
> assumption that structurally decreasing recursive functions will
> terminate on all inputs. The uses we found in the wild in OCaml
> programs typically combine “negative” constructs (functions, lazy,
> records) rather than infinite lists or trees.

by observing the preponderance of examples involving records,
functions and lazy values in `data/hard.log`.

### §2 Overview

The claims made in Section 2.1 and Section 2.2 are covered in more
detail in later sections.  We consequently focus here on Section 2.3,
showing that the OCaml compiler bugs described in that section have
been fixed by our check.  (We draw the reviewers' attention to the
instructions at the top of this file for switching between OCaml
versions 4.05.0 and 4.11.1+drecmodes):

  * **PR7231 unsoundness with nested recursive bindings**

  ```
  let rec r = let rec x () = r
                    and y () = x ()
               in y ()
         in r "oops" (* segfault *)
  ```

  Entering this program into the OCaml 4.05.0 top level causes a
  segmentation fault.

  In contrast, OCaml 4.11.1+dreccheck rejects the program.  The
  reviewers may like to start OCaml 4.11.1+dreccheck with the
  `-drecmodes` option to see the inferred environments for the various
  subexpressions:
  
  ```
  r:Delay |- fun () -> r : Return
  x:Delay |- fun () -> x () : Return
  r:Dereference |- let rec x () = r
                   and y () = x () in y () : Return
  ```
  The program is rejected because the recursively-bound variable `r` occurs
  with mode `Dereference` in the last environment.

  (Note: In the paper we mistakenly say that "`y ()` uses `r` at mode
   `Return`"; we will correct the error in the revised version.)

  * **PR7215: unsoundness with GADTs**

  ```
  type (_,_) eq = Refl : ('a,'a) eq;;
  let is_int (type a) =
    let rec (p : (int, a) eq) = match p with Refl -> Refl in p;;
  ```

  In OCaml 4.05.0 the program is accepted with the following type:

  ```
  val is_int : (int, 'a) eq = Refl
  ```

  In 4.11.1+drecmodes the program is rejected because `p` is used with
  mode `Dereference` as we claim in the paper:

  ```
  p:Dereference |- match p with | Refl -> Refl : Return
  ```

  * **PR6939: unsoundness with float arrays**

  ```
  let rec x = ([| x |]; 1.) in ()
  ```

  This program is rejected by our check, which infers the following environment:

  ```
  x:Dereference |- [|x|]; 1. : Return
  ```

  It's also (correctly) rejected by OCaml 4.05.0; the reviewers may,
  if they wish, observe it in an earlier version of OCaml such as
  4.02, which is also available in the image.

  * **PR4989: inconveniently rejected program**

  ```
  let rec f = let g = fun x -> f x in g
  ```

  In 4.05 this safe program is (inconveniently, as the original bug
  report observed) rejected:

  ```
  Error: This kind of expression is not allowed as right-hand side of `let rec'
  ```

  OCaml 4.11.1+drecmodes accepts the program, inferring the following
  mode environment:
  
  ```
  f:Delay |- let g x = f x in g : Return
  ```

### §4 Our inference rules for recursive definitions

#### Representative examples

At the time of preparing the artifact, the paper lacks simple examples
of how the mode judgment works. Our reviewers have asked for
representative exemples, and we will be adding them to the final
version of the paper.

We include the examples below, discussed in prose. They are reproduced
in the [examples/](examples/) subdirectory, with a comment for each
example listing the expected output when run with 4.11+drecmodes with
the -drecmodes flag.

1. 
   ```
     let rec x = x
   ```
   should be rejected.  Corresponding mode judgment:
   ```
       x: Return |- x : Return
   ```
   (`x` is stricter than `Guard` in the context, our check fails)

2. 
   ```
     let rec x = { self = x }
     let rec f = fun x -> f x
   ```
    should be accepted.  Corresponding mode judgments:
   ```
       x : Guard |- { self = x } : Return
       f : Delay |- ... : Return
   ```
       (our check passes)

3. 
   ```
     let rec x = g { self = x }
     let rec f = g (fun x -> f x)
   ```
   should be rejected (g may dereference the self field of its argument, call its argument).  Corresponding mode judgments:
   ```
       g: Dereference, x : Dereference |- g { self = x } : Return
       g: Dereference, f : Dereference |- g (fun x -> f x) : Return
   ```
    thanks to the mode composition rules
   ```
         Dereference[Guard] = Dereference
         Dereference[Delay] = Dereference
   ```
    (our check fails)

4. In contrast to the previous examples:
   ```
    let rec x = { self = g x }
   ```
   gives
    ```
     g: Dereference, x:Dereference |- { self = g x } : Return
    ```
   (check fails)
   from `Guard[Dereference] = Dereference`, but
    ```
     let rec f = fun x -> g (f x)
    ```
   gives
    ```
     g : Delay, f: Delay |- fun x -> g (f x) : return
    ```
     (checks passes)
   from `Delay[Dereference] = Delay`

5. For Ignore: notice that if we have
   ```
    x : Delay |- t : Return
   ```
   then we have
    ```
     x : Dereference, f : Dereference |- f t : Return
    ```
   but if we have
    ```
     x : Ignore |- t : Return
    ```
   then we still have
    ```
     x : Ignore, f : Dereference |- f t : Return
    ```
   So a declaration such as
    ```
     let rec x = f t
    ```
   would be rejected if
  ```
  (x : Delay |- t : Return),
  ```
   but accepted if
  ```
  (x : Ignore |- t : Return).
  ```

6. Examples for the "let" rule.  
   Example A:
  ```
    x: Delay |- let t = fun y -> (x, y) in { self = t } : Return
  ```
   we have
  ```
      x : Delay |- fun y -> (x, y)
  ```
    in the definition of t and
  ```
     t : Guard |- { self = t }
  ```
    so the final context is
  ```
      Guard[x : Delay] = x : Delay
  ```
   Example B:
  ```
    g: Dereference, x: Dereference |- let t = fun y -> (x, y) in g t : Return
  ```
    we have
  ```
      x : Delay |- fun y -> (x, y) : Return
  ```
    in the definition of t and
  ```
     t : Derefrence, g: Dereference |- g t : Return
  ```
    so the final context is
  ```
      Dereference[x : Delay], g:Dereference = x :Dereference, g:Dereference
  ```

7. Example with mutual recursion:
  ```
      let rec t = x
      and u = { self = t }
      and v = fun () -> (u, y)
  ```
   We have:
   ```
       x:Return |- x : Return                           (for t = ...)
       t:Guard |- { self = t } : Return                 (for u = ...)
       u:Delay, y:Delay |- fun () -> (u, y) : Return    (for v = ...)
   ```
   So we have to find Γ'₁, Γ'₂, Γ'₃ such that
   ```
       Γ'₁ = (x : Return) + ∅  (t does not use any mutually-recursive name)
       Γ'₂ = ∅ + Guard[Γ'₁]    (u uses t in Guard mode)
       Γ'₃ = (y : Delay) + Delay[Γ'₂]
   ```
     the fixpoint solution is
   ```
       Γ'₁ = x:Return
       Γ'₂ = x:Guard
       Γ'₃ = x:Delay, y:Delay
   ```
   For example we would have
   ```
       x:Return, y:Delay |-
         let rec t = x
         and u = { self = t }
         and v = fun () -> (u, y)
         in t
   ```
     rejected, but
   ```
       x:Guard, y:Delay |-
         let rec t = x
         and u = { self = t }
         and v = fun () -> (u, y)
         in u
      accepted.
   ```

#### Other claims

4.1 also claims several algebraic properties of mode composition:

 - that mode composition is associative (lines 358-359)
 - that mode composition is not commutative (lines 358-359)
 - that Ignore is the absorbing element of mode composition (line 365)
 - that Return is an identity (line 366)
 - that composition is idempotent (line 366)

These properties can be verified by finite enumeration, and are
"proved" in this way in the file `algebraic_proofs.ml` in our paper
repository, which we include in the image.

We refer the reviewers to `README.md` for further comments on the
correspondence between the system described in Section 4 and our
implementation in the OCaml compiler.

### §5 Meta-theory: soundness, §6 Global store semantics

No claims that can be evaluated by an artifact (there are mathematical
statements with proofs included in the paper appendices).

### §7 Extension to a full language

Reviewers may like to run OCaml 4.11.1+drecmodes with the `-drecmodes`
flag to see the results on the examples shown in the paper.

#### §7.1 The size discipline

The implementation of the check for statically-known sizes (lines
944-952) may be found here:

   https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml#L124-L257

Here is an example of a recursive definition that is rejected because
the bound expression does not have a statically-known size:

    let rec g = if true then let _ = g in Some 3 else None in g

Our implementation infers the following environment

    g:Guard |- if true then let _ = g in Some 3 else None : Return

which is safe, but since `Some 3` and `None` have different sizes in
memory the statically-known size check fails and the program is rejected.

#### §7.2 Dynamic representation checks: float arrays

OCaml 4.11.1+drecmodes rejects the example program (lines 977-979)

    let rec x = (let u = [|y|] in 10.5) and y = 1.5

inferring an environment in which elements used to construct a float
array are treated as dereferences (lines 973-974):

    y:Dereference |- let u = [|y|] in 10.5 : Return

#### §7.3 Exceptions and first-class modules

OCaml 4.11.1+drecmodes rejects the example program (lines 995-999)

    module type T = sig exception A of int end;;
    let rec x = (let module M = (val m) in M.A 42)
        and (m : (module T)) = (module (struct exception A of int end) : T)

inferring an environment in which unpacking the package variable `m`
is treated as a dereference:

    m:Dereference |- let module M = (val m) in M.A 42 : Return

### §7.4 GADTs

OCaml 4.11.1+drecmodes rejects the example program (lines 1016-1019):

    type (_,_) eq = Refl : ('a,'a) eq
    let universal_cast (type a) (type b) : (a, b) eq =
      let rec (p : (a, b) eq) = match p with Refl -> Refl in p

inferring an environment in which matching against the sole
constructor `Refl` is treated as a dereference:

    p:Dereference |- match p with | Refl -> Refl : Return

Reviewers may like to confirm that OCaml 4.05.0 incorrectly accepts
this unsound program.

### §7.5 Laziness

Reviewers may like to experiment with the behaviour of our check on
lazy expressions (lines 1030-1041); for example, the following code is
rejected (due to the lazy value optimization)

    let rec x = lazy y and y = 3

with the mode Return for `y` as our paper describes (lines 1043-1044):

     y:Return |- lazy y : Return

while the following more slightly complex program, which is not
subject to the lazy value optimization, is accepted:

    let rec x = lazy (y+0) and y = 3

with the following environment:

    Stdlib:Delay, y:Delay |- lazy (y + 0) : Return

(Here the entry for Stdlib in the environment refers to the module
 containing the definition of the `+` operator.)

### §8, §9 Related work, Conclusion

No claims that can be evaluated by an artifact
