(** First, some auxiliary definitions for lazy lists,
    with accompanying 'map2' (zipWith), 'nth' and 'tail' functions
*)
type 'a llist' = Nil | (::) of 'a * 'a llist
and 'a llist = 'a llist' Lazy.t

let rec map2 f x y =
  lazy (match x, y with
        | _, lazy Nil | lazy Nil, _ -> Nil
        | lazy ((x :: xs)), lazy ( (y :: ys)) -> ( (f x y :: map2 f xs ys)))

let rec nth n l =
  match n, l with
  | 0, lazy (x :: _) -> x
  | n, lazy (_ :: xs) -> nth (pred n) xs
  | _ -> invalid_arg "nth"

let tail x = lazy (match x with lazy (_::tl) -> Lazy.force tl
                              | _ -> invalid_arg "tail");;

(** The fib function from the paper, lines 19-20.
    Our recursive-binding check accepts this function.
 *)
let rec fib = fun x -> if x <= 1 then x
                       else fib (x-1) + fib (x-2)


(** The lfibs function from the paper, lines 24-25.
    Our recursive-binding check accepts this function.
 *)
let rec lfibs = lazy (1 :: lazy (1 :: map2 (+) lfibs (tail lfibs)))

(** The next example needs some more auxiliary definitions for memoization:
    a record type 'memo_table' and functions 'empty_table' and 'remember' *)
type ('k,'v) memo_table = { f: 'k -> 'v; values: ('k, 'v) Hashtbl.t }
let empty_table () = Hashtbl.create 10
let remember t k =
  try Hashtbl.find t.values k
  with Not_found -> let v = t.f k in Hashtbl.add t.values k v; v

(** The mfib function from the paper, lines 30-32.
    Our recursive-binding check accepts this function.
 *)
let rec mfib = fun x -> if x <= 1 then x
                        else remember mfibs (x-1) + remember mfibs (x-2)
   and mfibs = { f = fib; values = empty_table () }

(** The mfib' function from the paper, lines 36-39.
    Our recursive-binding check accepts this function.
 *)
let rec mfib' = let mfibs' = { f = mfib'; values = empty_table () } in
                fun x -> if x <= 1 then x
                         else remember mfibs' (x-1) + remember mfibs' (x-2)

(** The efibs example from the paper, line 45.
    Our recursive-binding check REJECTS this function.
 *)
open Stdlib
open List (* Bring eager standard library definitions back into scope *)

let rec efibs = 0 :: 1 :: map2 (+) efibs (tl efibs) (* unsafe! *) 


let () = print_endline "OK!"
