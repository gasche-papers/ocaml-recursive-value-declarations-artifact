This repository contains the artifact that we are submitting alongside
a POPL paper named "A simple mode system for recursive definitions".

The paper studies a specific feature of some functional programming
languages, namely recursive definitions of things that are not
immediate functions (recursive records, functions preceded by local
declarations or various other language constructs, etc.). We propose
a new system of inference rules to characterize valid definitions, to
avoid runtime errors when evaluating the definitions. An algorithm can
be directly derived from our inference rules to check the validity of
recursive definitions. The check has been integrated in the OCaml
compiler -- more precisely, the implementation work for the OCaml
compiler led to the present paper.


# Verifiable claims

Our paper contains various verifiable claims on different aspects of
our work, for example:

1. To justify the study of recursive definitions, we analyzed
   recursive definitions in a large corpus of OCaml programs, pointing
   out that many programs used recursive definitions outside the easy
   fragment of immediate functions.

2. The paper provides examples of how the system of inference rules
   works; those examples can be compared to our algorithm.

3. We claimed that the correctness test used by OCaml before our work
   had various issues, which can be verified.

An exhaustive list of claims, as recommended by the Artifact
Evaluation instructions, is present in [CLAIMS.md](CLAIMS.md).

# Download, installation, and sanity-testing instructions

The artifact is a Docker image.  The source files are in this
directory.  The image may be downloaded from the following URL:

    https://zenodo.org/record/4075115

We have tested it with Docker version 18.09.2, but we expect it to
work with any recent version of Docker.

The following steps may be followed to test that everything is working:

 1. Unpack the image using 'docker load':

       docker load < ocaml-recursive-values.tar.gz

    When this succeeds Docker displays the image name:

       Loaded image: ocaml-recursive-values:latest

 2. Run the Docker image in interactive mode:

       docker run -it --rm ocaml-recursive-values

    You should see a prompt like this:

       opam@36a8ac6444a8:~

    (The container id, here 36a8ac6444a8, will vary from run to run.)

 3. Try switching to our instrumented version of OCaml

       opam switch 4.11.1+drecmodes
       eval $(opam env)
       
 4. Start the OCaml top level (passing the `-drecmodes` argument) and
    enter a simple recursive definition

    OCaml will print out both the inferred mode environments and the
    inferred types:

       opam@36a8ac6444a8:~$ ocaml -drecmodes
       OCaml version 4.11.1

       # let rec x = [y] and y = lazy (`A x);;
      y:Guard |- [y] : Return
      x:Delay |- lazy (`A x) : Return
       val x : [> `A of 'a ] lazy_t list as 'a = [<lazy>]
      val y : [> `A of 'a list ] lazy_t as 'a = <lazy>

# Evaluation instructions

We provide evaluation instructions alongside each claim in
[CLAIMS.md](CLAIMS.md)

# A central artifact-related question

In our view, the following is the key artifact-related question about
our work:

> Do the formal system presented in the paper and the implementation
> in OCaml compiler correspond to each other?

This is not a claim that can be *reproduced* (running examples is not
enough), but it can be *evaluated* by informally comparing the
inference rules in the paper (Figure 3) with the algorithm
implementation in the OCaml compiler:

https://github.com/ocaml/ocaml/blob/4.11.1/typing/rec_check.ml

There is inevitably some distance between the paper, which works on an
untyped "toy language" with only the salient features required to
justify the various aspects of our work, and the implementation on the
full OCaml language. We cannot expect a formal code review (just like
paper-proof reviews are out of scope), but we would be interested in
our evaluators' informal opinion on whether the two seem to match each
other reasonably well.

We gathered some remarks, in case some artifact evaluators are
interested in looking at this, in [CODE_MODEL_COMPARISON.md](CODE_MODEL_COMPARISON.md).
